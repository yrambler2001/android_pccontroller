package yrambler2001.pccontroller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;

import mehdi.sakout.fancybuttons.FancyButton;

import static yrambler2001.pccontroller.MainActivity.dictt;
import static yrambler2001.pccontroller.Util.hashMap;
import static yrambler2001.pccontroller.Util.send;

@SuppressLint("SetTextI18n")
public class PowerFragment extends Fragment {

    public static WeakReference<Context> powerfragment;
    static char numClass=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.power_lay, container, false);
        powerfragment = new WeakReference<>(view.getContext());

        FancyButton btn_power = view.findViewById(R.id.btn_power);
        btn_power.setOnClickListener(this::toggle);
        view.findViewById(R.id.fabDownload).setOnClickListener(v -> send(view.getContext(), hashMap("doit", "info"), numClass));
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Power");
        send(view.getContext(), hashMap("doit", "info"), numClass);
    }

    public void toggle(View view) {
        if (view.isEnabled()) {
            send(MainActivity.thisContext, hashMap("power", "toggle"), numClass);
        }
    }

    static void update() {
        if (powerfragment != null) {
            Context context = powerfragment.get();
            FancyButton btn_power = ((Activity) context).findViewById(R.id.btn_power);
            String resp = dictt.get("PC");
            if (resp != null) {
                boolean isOn = resp.contentEquals("1");
                btn_power.setBackgroundColor(Color.parseColor(isOn ? "#2e7d32" : "#c62828"));
                btn_power.setFocusBackgroundColor(Color.parseColor(isOn ? "#60ad5e" : "#ff5f52"));
            }
            btn_power.setClickable(true);
            btn_power.setEnabled(true);
        }

    }


}
