package yrambler2001.pccontroller;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.HashMap;

import static yrambler2001.pccontroller.MainActivity.dictt;
import static yrambler2001.pccontroller.Util.hashMap;
import static yrambler2001.pccontroller.Util.prepareLay;
import static yrambler2001.pccontroller.Util.send;

@SuppressLint("SetTextI18n")
public class FanFragment extends GridFragment {
    static char numClass = 1;

    {
        title = "Fan";
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.grid_lay, container, false);

        String[] namesArray = getResources().getStringArray(R.array.fan_names);
        int[] fan_max = getResources().getIntArray(R.array.fan_max);
        int[] fan_min = getResources().getIntArray(R.array.fan_min);
        int[] fan_curr = getResources().getIntArray(R.array.fan_curr);
        count=fan_curr.length;
        View[][] views = prepareLay(count, view, inflater, container, namesArray, fan_max, fan_min, fan_curr);
        table = (TableLayout) views[0][0];
        row = (TableRow[]) views[1];
        textView = (TextView[]) views[2];
        seekBar = (DiscreteSeekBar[]) views[3];
        editText = (EditText[]) views[4];

        view.findViewById(R.id.fabDownload).setOnClickListener(v -> send(getContext(), hashMap("doit", "info"), numClass));
        view.findViewById(R.id.fabUpload).setOnClickListener(view1 -> {
            HashMap<String,String> hmFans=new HashMap<>();
            for (int i = 0; i < count; i++)
                hmFans.put("fan"+i,seekBar[i].getProgress()+"");
            send(getContext(), hmFans, numClass);
        });
        update();
        return view;
    }


    static void update() {
        if (!dictt.isEmpty()) {
            String resp = dictt.get("fan");
            if (resp != null) {
                String[] r=resp.split("A");
                for (int i=0;i<r.length;i++)
                    editText[i].setText(r[i]+"");

            }
        }
    }


}
