package yrambler2001.pccontroller;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
@SuppressLint("SetTextI18n")
public class GridFragment extends Fragment {
    public static int count;
    public String title;
    public TableLayout table;
    public static EditText[] editText;
    public TableRow[] row;
    public TextView[] textView;
    public DiscreteSeekBar[] seekBar;



@Override
public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    getActivity().setTitle(title);
}
}
